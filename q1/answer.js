let filename = process.argv[2],
    reverseWords = require('./reverse-words'),
    readFilePromise = require('../read-file-promise'),
    reversedFileWords = file => {
        return readFilePromise( `${__dirname}/${file}` );
    };

if (process.argv.length < 3) {
  console.log(`Usage: node ${process.argv[1]} FILENAME`);
  process.exit(1);
}

reversedFileWords( filename )
    .then( text => {
        let textLineArr = text.split('\n');
        console.log( reverseWords( textLineArr ) );
    } )
    .catch( err => console.log(err) );