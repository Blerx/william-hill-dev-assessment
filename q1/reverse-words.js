module.exports = arr => {
    let msg = '';
    if( Array.isArray(arr) ) {
        for( let item of arr ) {
            // Reverse string/add new line if not last item
            msg += `${item.split(' ').reverse().join(' ')}${ arr[arr.length - 1] === item ? '' : '\n' }`;
        }
        return msg;
    } else {
        let msg = `Expected an Array but got a ${ typeof arr }`;
        return msg;
    }
}