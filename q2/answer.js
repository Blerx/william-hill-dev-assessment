let filename = process.argv[2],
    change = require('./change'),
    _ = require('underscore'),
    readFilePromise = require('../read-file-promise'),
    getPossibleCombinations = file => {
        return readFilePromise( `${__dirname}/${file}` );
    };

if (process.argv.length < 3) {
  console.log(`Usage: node ${process.argv[1]} FILENAME`);
  process.exit(1);
}

getPossibleCombinations( filename )
    .then( text => {
        let totals = text.split('\n').map( text => {
            if ( text.indexOf('£') !== -1 ){
                return parseInt( text.replace('£', '') ) * 100;
            } else if ( _.isNaN( parseInt(text) ) ) {
                return
            }

            return parseInt( text )
        });
        for( let total of totals ) {
            console.log( change(total, [1, 2, 5, 10, 20, 50, 100, 200]) )
        }
    } )
    .catch( err => console.log(err) );