module.exports = ( total, coins ) => {
    if( typeof total  === 'number' && Array.isArray( coins ) ) {
        let count = 0,
            change = (index, amount) => {
                let currentCoin = coins[ index ];

                if( index === 0 ){
                if( amount % currentCoin === 0 ) {
                    count++;
                }
                return;
                }

                while( amount >= 0 ){
                change( index-1, amount );
                amount -= currentCoin;
                }
            }

        change( coins.length-1, total );
        return count;
    } else {
        let msg = `Expected a Number and an Array but got a ${ typeof total } and a ${ Array.isArray( coins ) ? 'array ' : typeof coins }`;
        return msg;
    }
};