let fs = require('fs');

module.exports = fs.readFileAsync = file => {
    return new Promise( (resolve, reject) => {
        fs.readFile( file, 'utf8', function(err, data) {
            if (err) 
                reject(err); 
            else 
                resolve(data);
        });
    });
};