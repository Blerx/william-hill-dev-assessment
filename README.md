## Usage

node q<num>/answer <input-file>
This assumes the input file is in the same directory as answer.js

## Example

node q1/answer input.txt

## Test

npm test

## Design Decisions

For Q1 I decided to use a manual **Promise** for grabbing the input file data and gave a few **test** examples.
For Q2 I had read up on how to do this and opted for a recursive solution over an iterative one prevent hard coding the denominations.

## Improvements

- I would write more tests for the whole implementation not just the reverse-words and change exports.
- I would refactor the code and find a **pure** way to do the change function as it has **side effects**.
- Also a good idea would be for be able to use floats instead of just integers in Q2
- Lots could also be done to make things clearer