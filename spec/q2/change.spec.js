var change = require('../../q2/change');

describe('Q1 reverse ord in list', () => {
    it('should integer and an array', () => {
        expect( change('string') ).toContain( 'Expected an Number and an Array' );
    } )

    it('should provide number of possible combinations for total', () => {
        expect( change( 3, [1, 2, 5, 10, 20, 50, 100, 200] ) ).toBe( 2 );
    } )
} )