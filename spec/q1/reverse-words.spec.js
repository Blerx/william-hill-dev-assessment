var rw = require('../../q1/reverse-words');

describe('Q1 reverse ord in list', () => {
    it('should only accept a list', () => {
        expect( rw('string') ).toContain( 'Expected an Array' );
    } )

    it('should print item words in reverse', () => {
        expect( rw( ['bar foo'] ) ).toBe('foo bar');
    } )

    it('should out put each item on new line ', () => {
        expect( JSON.stringify( rw( ['bar foo','foo bar'] ) ) ).toBe(`"foo bar\\nbar foo"`);
    } )
} )